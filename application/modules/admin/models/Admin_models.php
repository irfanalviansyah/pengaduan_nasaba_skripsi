<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_models extends CI_Model
{
    public function read_pengaduan($where = '', $order = '')
    {
        if (!empty($where)) $this->db->where($where);
        if (!empty($order)) $this->db->order_by($order);

        $this->db->select('p.id as id_pengaduan_m, pu.id as id_pengaduan_user, p.no_nasabah, u.nama_lengkap, p.subject, p.message, pu.status, p.date_created');
        $this->db->from('pengaduan as p');
        $this->db->join('pengaduan_user as pu', 'p.id = pu.id_pengaduan', 'left');
        $this->db->join('user as u', 'p.no_nasabah = u.no_nasabah', 'left');

        $query = $this->db->get();
        if ($query and $query->num_rows() != 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function read_balasan($where = '', $order = '')
    {
        if (!empty($where)) $this->db->where($where);
        if (!empty($order)) $this->db->order_by($order);

        $this->db->select('pb.id as id_pengaduan_balasan, u.id as id_user, pb.id_pengaduan_user , u.nama_lengkap, pb.reply, pb.date_reply');
        $this->db->from('pengaduan_balasan as pb');
        $this->db->join('user as u', 'pb.id_user = u.id');

        $query = $this->db->get();
        if ($query and $query->num_rows() != 0) {
            return $query->result();
        } else {
            return array();
        }
    }
	
	public function grafik_total(){
		$this->db->select('date_format(date(date_created), "%a, %d-%M-%Y") as nrm_date , count(*) as cnt ');
		$this->db->from('pengaduan');
		$this->db->group_by('nrm_date');
		
		$query = $this->db->get();
		if ($query and $query->num_rows() != 0) {
            return $query->result();
        } else {
            return array();
        }
	}
}
