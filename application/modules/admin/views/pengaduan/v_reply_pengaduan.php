<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reply pengaduan <?php echo $result->subject ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('admin') ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('admin/pengaduan') ?>">Pengaduan</a></li>
                        <li class="breadcrumb-item active">Reply</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?php echo site_url('admin/pengaduan') ?>" class="btn btn-primary btn-block mb-3">Kembali ke pengaduan</a>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Informasi Nasabah</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <ul class="nav nav-pills flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-credit-card"></i> No Nasabah
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <?php echo $result->no_nasabah ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-user-circle"></i> Nama Nasabah
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <?php echo $result->nama_lengkap ?>
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <i class="fas fa-inbox"></i> Subject
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <?php echo $result->subject ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-envelope"></i> Message
                                    </a>
                                </li>
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <?php echo $result->message ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Information</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <ul class="nav nav-pills flex-column">
                                <li class="nav-item active">
                                    <a href="#" class="nav-link">
                                        <i class="fas fa-inbox"></i> Balasan
                                        <span class="badge bg-primary float-right">1</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-envelope"></i> Status pengaduan
                                        <span class="badge bg-primary float-right"><?php echo $result->status ?></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <?php if ($result->status == "Closed") { ?>
                    <?php } else { ?>
                    <a href="<?php echo site_url('admin/pengaduan/do_close_pengaduan/'.$result->id_pengaduan_user) ?>" class="btn btn-danger btn-block mb-3">Tutup status pengaduan</a>
                    <?php } ?>
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <!-- /.card -->
                    <?php if ($result->status == "Closed") { ?>
                    <?php } else { ?>
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Form Balasan</h3>
                        </div>
                        <!-- /.card-header -->
                        <form action="<?php echo site_url('admin/pengaduan/do_reply_pengaduan/do_reply') ?>" method="post">
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id') ?>">
                                    <input type="hidden" name="id_pengaduan_m" value="<?php echo $result->id_pengaduan_m ?>">
                                    <input type="hidden" name="id_pengaduan_user" value="<?php echo $result->id_pengaduan_user ?>">
                                    <input type="hidden" name="no_nasabah" value="<?php echo $result->no_nasabah ?>">
                                    <textarea name="balasan" id="compose-textarea" class="form-control" style="height: 200px" placeholder="Masukan Balasan Anda"></textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                                </div>
                            </div>
                        </form>
                        <!-- /.card-footer -->
                    </div>
                    <?php } ?>

                    <?php foreach ($balasan as $row) { ?>
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title"><?php echo $row->nama_lengkap ?></h3>
                                <?php if ($row->id_user == $this->session->userdata('id')) { ?>
                                    <div class="card-tools">
                                        <a type="button" href="<?php echo site_url('admin/pengaduan/do_delete_reply/' . $row->id_pengaduan_balasan) ?>" class="btn btn-danger btn-block"><i class="fas fa-window-close"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group">
                                    <p><?php echo $row->reply ?></p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <p><?php echo $row->date_reply ?></p>
                                </div>
                            </div>
                            <!-- /.card-footer -->
                        </div>
                    <?php } ?>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>