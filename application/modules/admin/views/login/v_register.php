<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="#"><b>PT. Gotong Royong Bisnis Indonesia</a>
    </div>

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Register Member baru</p>

        <form action="<?php echo site_url('login/register/do_register')?>" method="post">
          <div class="input-group mb-3">
            <input name = "no_nasabah" type="number" class="form-control" placeholder="Nomor Nasabah"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="8">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-credit-card"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input name = "nama_lengkap" type="text" class="form-control" placeholder="Nama Lengkap">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <textarea name = "alamat" type="text" class="form-control" placeholder="Alamat" rows="4"></textarea>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-location-arrow"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input name = "no_telp" type="number" class="form-control" placeholder="Nomor Telpon"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="12">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-phone"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input name="email" type="email" class="form-control" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input name = "password" type="password" id="password" class="form-control" placeholder="Password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" id="confirm_password" class="form-control" placeholder="Retype password" onkeyup="validation();">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- /.col -->
            <div class="col-12">
              <p id="message"></p>
              <button disabled id="btn_submit" type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <br>

        <a href="<?php echo site_url('login') ?>" class="text-center">Saya sudah menjadi member</a>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
  <!-- /.register-box -->