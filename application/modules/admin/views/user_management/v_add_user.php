<?php
if (@$form_edit) {
	$url = "admin/user_management/do_edit_user/".@$res->id;
} else {
	$url = "admin/user_management/add_user/do_add_user";
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Tambah user</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo site_url('admin') ?>">Home</a></li>
						<li class="breadcrumb-item"><a href="<?php echo site_url('admin/user_management') ?>">User Management</a></li>
						<li class="breadcrumb-item active">Add User</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<form action="<?php echo site_url($url) ?>" method="POST">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Form tambah user</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fas fa-minus"></i></button>
							</div>
						</div>
						<div class="card-body">


							<div class="form-group">
								<label for="inputStatus">Role</label>
								<select name="role" id="role" onchange="roleFunction()" class="form-control custom-select" required <?php if (@$res->role == "User") { ?>disabled<?php } ?>>
									<?php if ($res->role == "User") { ?>
										<option value="" disabled>Pilih salah satu</option>
										<option value="User" selected>User</option>
										<option value="Admin">Admin</option>
									<?php } elseif ($res->role == "Admin") { ?>
										<option value="" disabled>Pilih salah satu</option>
										<option value="User">User</option>
										<option value="Admin" selected>Admin</option>
									<?php } else { ?>
										<option value="" selected disabled>Pilih salah satu</option>
										<option value="User">User</option>
										<option value="Admin">Admin</option>
									<?php } ?>
								</select>
							</div>
							<div id="DivNasabah" class="form-group" <?php if (@$res->role == "Admin") { ?>style="display: none;" <?php } ?>>
								<label for="inputName">Nomor Nasabah</label>
								<input value="<?php echo @$res->no_nasabah ?>" required <?php if (@$res->role == "User") { ?>disabled<?php } ?> type="number" name="no_nasabah" id="no_nasabah" class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="8">
								<br>
								<div class="row">
									<div class="col-sm-6">
										<button type="button" class="btn btn-primary btn-block" onclick="numberGenerator()" <?php if (@$res->role == "User") { ?>disabled<?php } ?>>Generate Nomor Nasabah</button>
									</div>
									<div class="col-sm-6">
										<div class="float-sm-right">
											<p>*Untuk Nomor nasabah dapat dimasukan manual atau generate menggunakan tombol</p>
											<p>*Maksimal 8 Digit</p>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="inputClientCompany">Nama lengkap</label>
								<input value="<?php echo @$res->nama_lengkap ?>" type="text" name="nama_lengkap" id="inputClientCompany" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="inputDescription">Alamat</label>
								<textarea id="inputDescription" name="alamat" class="form-control" rows="4"><?php echo @$res->alamat ?></textarea>
							</div>
							<div class="form-group">
								<label for="inputName">No Telpon</label>
								<input value="<?php echo @$res->no_telp ?>" required type="number" name="no_telp" id="no_nasabah" class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="12">
							</div>
							<div class="form-group">
								<label for="inputProjectLeader">Email</label>
								<input value="<?php echo @$res->email ?>" required type="email" name="email" id="inputProjectLeader" class="form-control">
							</div>
							<div class="form-group" <?php if (!@$form_edit) { ?> style="display: none" <?php } ?>>
								<div class="row">
									<div class="col-sm-2">
										<button type="button" onclick="changePass()" class="btn btn-primary btn-block">Ganti Password</button>
									</div>
									<div class="col-sm-10">
										<div id="divBtnCancelPass" class="float-sm-right" style="display: none">
											<button type="button" onclick="closePass()" class="btn btn-danger btn-block">Cancel</button>
										</div>
									</div>
								</div>
							</div>
							<div id="divPassword" <?php if (@$form_edit) { ?>style="display:none" <?php } ?>>
								<div class="form-group">
									<label for="inputProjectLeader">Password</label>
									<input <?php if (!@$form_edit) { ?> required <?php } ?> type="password" name="password" id="password" class="form-control">
								</div>
								<div class="form-group">
									<label for="inputProjectLeader">Ketik Ulang Password</label>
									<input <?php if (!@$form_edit) { ?> required <?php } ?> type="password" id="confirm_password" class="form-control" onkeyup="validation();">
									<br>
									<p id="message"></p>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<a href="<?php echo site_url('admin/user_management') ?>" class="btn btn-secondary">Kembali</a>
					<input <?php if (!@$form_edit) { ?>disabled<?php } ?> type="submit" id='btn_submit' value="Create user" class="btn btn-success float-right">
				</div>
			</div>
		</form>
		<br>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->