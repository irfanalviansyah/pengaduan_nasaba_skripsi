<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User Management</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin') ?>">Home</a></li>
            <li class="breadcrumb-item active">user management</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="card-title">Table user yang terdaftar</h3>
              </div>
              <div class="col-sm-6">
                <div class="float-sm-right">
                  <a href="<?php echo site_url('admin/user_management/add_user') ?>" class="btn btn-primary btn-block">Tambah User</a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="dt_user" class="table table-bordered table-striped nowrap">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Nasabah</th>
                  <th>Nama Lengkap</th>
                  <th>Alamat</th>
                  <th>No telp</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Tanggal dibuat</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
			  <tfoot>
			  <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Role</th>
                  <th>Tanggal dibuat</th>
                  <th></th>
                </tr>
			  </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->