<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaduan extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama_lengkap');
		if (empty($user)) redirect("auth");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
		$this->load->model('admin_models');
	}

	public function index()
	{
		$data['view'] = 'pengaduan/v_tbl_pengaduan';
		$this->load->view("index", $data);
	}

	public function dt_pengaduan()
	{
		$data = $this->admin_models->read_pengaduan('', 'id_pengaduan_m DESC');
		$result = array();
		$i = 1;
		foreach ($data as $row) {
			$d = array();
			$d[] = $i++;
			$d[] = $row->no_nasabah;
			$d[] = $row->nama_lengkap;
			$d[] = $row->subject;
			$d[] = $row->message;
			//if ($row->status == "Open") {
				//$d[] = '<button type="button" class="btn btn-danger btn-block">' . $row->status . '</button>';
			//} else if ($row->status == "User Reply") {
				//$d[] = '<button type="button" class="btn btn-warning btn-block">' . $row->status . '</button>';
			//} else if ($row->status == "Admin Reply") {
				//$d[] = '<button type="button" class="btn btn-primary btn-block">' . $row->status . '</button>';
			//}
			$d[] = $row->status;
			$d[] = date_format(new DateTime($row->date_created), 'd-M-Y');
			$d[] = '<a href = "' . site_url('admin/pengaduan/redir/reply_pengaduan/' . $row->id_pengaduan_m) . '" class="btn btn-warning btn_block">Lihat Percakapan</a>';

			$d["DT_RowId"] = $row->id_pengaduan_m;
			$result['data'][] = $d;
		}

		if (empty($result)) $result = array("aaData" => []);
		echo json_encode($result);
	}

	public function update_status($id)
	{
		$data_pengaduan = $this->admin_models->read_pengaduan('p.id = ' . $id);
		$status = $data_pengaduan[0]->status;
		if ($status == "Open") {
			$data = array(
				'status' => "Open"
			);

			$this->db->trans_start();
			$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $id, $data);
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();
			if ($trans_status == FALSE) {
				$this->db->trans_rollback();
				//$this->session->set_flashdata('error', 'Gagal input Project');
				//redirect('home/admin/form_project');
				print "kesalahan database";
			} else {
				$this->db->trans_commit();
				//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
				redirect('admin/pengaduan/reply_pengaduan/' . $id);
			}
		} else {
			redirect('admin/pengaduan/reply_pengaduan/' . $id);
		}
	}

	public function do_close_pengaduan($id){
		$data_status = array(
			'status' => "Closed"
		);

		$this->db->trans_start();
		$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $id, $data_status);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('admin/pengaduan');
		}
	}

	public function reply_pengaduan($id)
	{
		$data_pengaduan = $this->admin_models->read_pengaduan('p.id = ' . $id);
		$data['balasan'] = $this->admin_models->read_balasan('id_pengaduan_user = ' . $data_pengaduan[0]->id_pengaduan_user, 'pb.id DESC');

		$data['result'] = $data_pengaduan[0];
		$data['view'] = 'pengaduan/v_reply_pengaduan';
		$this->load->view("index", $data);
	}

	public function do_reply_pengaduan()
	{
		$post = $this->input->post(NULL, TRUE);
		$data = array(
			'id_user' => $post['id_user'],
			'id_pengaduan_user' => $post['id_pengaduan_user'],
			'reply' => $post['balasan']
		);
		$data_status = array(
			'status' => "Admin Reply"
		);

		$this->db->trans_start();
		$this->main_models->main_create('pengaduan_balasan', $data);
		$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $post['id_pengaduan_user'], $data_status);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('admin/pengaduan/reply_pengaduan/' . $post['id_pengaduan_m']);
		}
	}

	public function do_delete_reply($id)
	{
		$this->db->trans_start();
		$this->main_models->main_delete('pengaduan_balasan', 'id = ' . $id);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
