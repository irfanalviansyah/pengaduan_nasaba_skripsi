<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_management extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama_lengkap');
		if (empty($user)) redirect("login");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
	}

	public function index()
	{
		$data['view'] = 'user_management/v_tbl_user';
		$this->load->view("index", $data);
	}

	public function dt_user()
	{
		$data = $this->main_models->main_read('user');
		$result = array();
		$i = 1;
		foreach ($data as $row) {
			$d = array();
			$d[] = $i++;
			$d[] = $row->no_nasabah;
			$d[] = $row->nama_lengkap;
			$d[] = $row->alamat;
			$d[] = $row->no_telp;
			$d[] = $row->email;
			$d[] = $row->role;
			$d[] = date_format(new DateTime($row->created), 'd-M-Y');
			$d[] = '<a href = "'.site_url('/admin/user_management/edit_user/'.$row->id.'/'.$row->no_nasabah).'" class="btn btn-primary btn_block">Edit</a> <a href = "'.site_url('admin/user_management/do_delete_user/'.$row->id).'" class="btn btn-danger btn_block">Delete</a>';

			$d["DT_RowId"] = $row->id;
			$result['data'][] = $d;
		}

		if (empty($result)) $result = array("aaData" => []);
		echo json_encode($result);
	}

	public function add_user()
	{
		$data['view'] = 'user_management/v_add_user';
		$this->load->view("index", $data);
	}

	public function do_add_user()
	{
		$post = $this->input->post(NULL, TRUE);

		$role = $post['role'];
		if ($role == "Admin") {
			$no_nasabah = "00000001";
		} else {
			$no_nasabah = $post['no_nasabah'];
		}
		$nama_lengkap = $post['nama_lengkap'];
		$alamat = $post['alamat'];
		$no_telpon = $post['no_telp'];
		$email = $post['email'];
		$password = md5($post['password']);

		$data = array(
			'no_nasabah' => $no_nasabah,
			'nama_lengkap' => $nama_lengkap,
			'alamat' => $alamat,
			'no_telp' => $no_telpon,
			'password' => $password,
			'role' => $role,
			'email' => $email
		);

		$this->db->trans_start();
		$this->main_models->main_create('user', $data);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('admin/user_management');
		}

		print $data;
	}

	public function edit_user($id)
	{
		$result = $this->main_models->main_read('user', 'id = ' . $id);
		$data['res'] = $result[0];
		$data['form_edit'] = TRUE;

		$data['view'] = 'user_management/v_add_user';
		$this->load->view("index", $data);
	}

	public function do_edit_user($id)
	{
		$post = $this->input->post(NULL, TRUE);

		// $role = $post['role'];
		// if ($role == "Admin") {
		// 	$no_nasabah = "00000001";
		// } else {
		// 	$no_nasabah = $post['no_nasabah'];
		// }
		$nama_lengkap = $post['nama_lengkap'];
		$alamat = $post['alamat'];
		$no_telpon = $post['no_telp'];
		$email = $post['email'];
		$password = md5($post['password']);

		if(!empty($password)){
			$data = array(
				//'no_nasabah' => $no_nasabah,
				'nama_lengkap' => $nama_lengkap,
				'alamat' => $alamat,
				'no_telp' => $no_telpon,
				'password' => $password,
				//'role' => $role,
				'email' => $email
			);
		}else{
			$data = array(
				//'no_nasabah' => $no_nasabah,
				'nama_lengkap' => $nama_lengkap,
				'alamat' => $alamat,
				'no_telp' => $no_telpon,
				//'role' => $role,
				'email' => $email
			);
		}

		$this->db->trans_start();
		$this->main_models->main_update('user', 'id = '.$id, $data);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('admin/user_management');
		}
	}

	public function do_delete_user($id){
		$result = $this->main_models->main_read('user', 'id = '.$id);

		$this->db->trans_start();
		$this->main_models->main_delete('user', 'id = '.$id);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('admin/user_management');
		}
	}
}
