<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama_lengkap');
		$role = $this->session->userdata('role');
		if (empty($user)) redirect("login");
		if($role == "User") redirect('user');
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
		$this->load->model('admin_models');
	}

	public function index() {
		$data['c_total_pengaduan'] = $this->main_models->count('pengaduan_user','');
		$data['c_total_pengaduan_open'] = $this->main_models->count('pengaduan_user','status = "Open"');
		$data['c_total_pengaduan_user_reply'] = $this->main_models->count('pengaduan_user','status = "User Reply"');
		$data['c_total_pengaduan_admin_reply'] = $this->main_models->count('pengaduan_user','status = "Admin Reply"');
		$data_grafik = $this->admin_models->grafik_total();
		$data['grafik_total'] = json_encode($data_grafik);
		$data['view'] = 'v_admin';
		$this->load->view("index", $data);
	}

	public function table_berita(){
		$data['view'] = 'v_tbl_berita';
		$this->load->view("index", $data);
	}
}