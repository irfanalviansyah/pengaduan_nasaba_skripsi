<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$user = $this->session->userdata('username');
		//if (empty($user)) redirect("auth");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
	}

	public function index()
	{
		$data['view'] = 'login/v_login';
		$this->load->view("index", $data);
	}

	public function do_login()
	{
		$post = $this->input->post(NULL, TRUE);

		$email = $post['email'];
		$password = md5($post['password']);

		$ver = $this->main_models->main_read('user', 'email = "' . $email . '" AND password = "' . $password . '"');
		$id = $ver[0]->id;
		if ($ver) {
			$data = array(
				'status' => 'Online',
				'last_login' => date("Y-m-d H:i:s")
			);
			$query = $this->main_models->main_update('user', 'id = ' . $id, $data);

			if ($query) {
				$this->session->set_userdata((array) $ver[0]);
				if ($ver[0]->role == "Admin") {
					redirect('admin');
				} else {
					redirect('user');
				}
			} else {
				$this->session->set_flashdata('error', 'Terjadi kesalahan sistem');
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('error', 'Username atau Password salah');
			redirect('login');
		}
	}

	public function do_logout($id)
	{
		if (empty($id)) {
			$this->session->sess_destroy();
			$this->session->unset_userdata("no_nasabah");
			redirect('login');
		} else {
			$ver = $this->main_models->main_read('user', 'id = ' . $id);
			$id = $ver[0]->id;
			$data = array(
				'status' => 'Offline',
				'last_login' => date("Y-m-d H:i:s")
			);
			$query = $this->main_models->main_update('user', 'id = ' . $id, $data);
			if ($query) {
				$this->session->sess_destroy();
				$this->session->unset_userdata("no_nasabah");
				redirect('login');
			}
			$this->session->sess_destroy();
			$this->session->unset_userdata("no_nasabah");
			redirect('login');
		}
	}

	public function register()
	{
		$data['view'] = 'login/v_register';
		$this->load->view("index", $data);
	}

	public function do_register()
	{
		$post = $this->input->post(NULL, TRUE);

		$no_nasabah = $post['no_nasabah'];
		$nama_lengkap = $post['nama_lengkap'];
		$alamat = $post['alamat'];
		$no_telpon = $post['no_telp'];
		$email = $post['email'];
		$password = md5($post['password']);

		$data = array(
			'no_nasabah' => $no_nasabah,
			'nama_lengkap' => $nama_lengkap,
			'alamat' => $alamat,
			'no_telp' => $no_telpon,
			'password' => $password,
			'role' => 'User',
			'email' => $email
		);

		$this->db->trans_start();
		$this->main_models->main_create('user', $data);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('login');
		}
	}
}
