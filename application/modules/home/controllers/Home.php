<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$user = $this->session->userdata('username');
		//if (empty($user)) redirect("auth");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
	}

	public function index()
	{
		$data['view'] = 'v_home';
		$this->load->view("index", $data);
	}

	public function api_get_no_nasabah()
	{
		$post = $this->input->post(NULL, TRUE);
		$no_nasabah = $post['no_nasabah'];
		$result = $this->main_models->main_read('user', 'no_nasabah = ' . $no_nasabah);
		echo @$result[0]->no_nasabah;
	}

	public function do_post_pengaduan()
	{
		$post = $this->input->post(NULL, TRUE);

		$no_nasabah = $post['no_nasabah'];
		$subject = $post['subject'];
		$message = $post['message'];

		$data_pengaduan = array(
			'no_nasabah' => $no_nasabah,
			'subject' => $subject,
			'message' => $message
		);

		$this->db->trans_start();
		$this->main_models->main_create('pengaduan', $data_pengaduan);
		$last_id = $this->db->insert_id();
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$data_user_pengaduan = array(
				'id_pengaduan' => $last_id,
				'no_nasabah' => $no_nasabah,
				'status' => 'Open'
			);

			$this->db->trans_start();
			$this->main_models->main_create('pengaduan_user', $data_user_pengaduan);
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();

			if ($trans_status == FALSE) {
				$this->db->trans_rollback();
				//$this->session->set_flashdata('error', 'Gagal input Project');
				//redirect('home/admin/form_project');
				print "kesalahan database";
			} else {
				$this->db->trans_commit();
				//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
				redirect('home');
			}
		}
	}
}
