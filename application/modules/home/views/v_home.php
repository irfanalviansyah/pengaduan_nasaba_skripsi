<!--==========================
    Intro Section
  ============================-->
<section id="intro" class="clearfix">
  <div class="container d-flex h-100">
    <div class="row justify-content-center align-self-center">
      <div class="col-md-6 intro-info order-md-first order-last">
        <h2>Selamat Datang Di <br>Website Pelayanan <span>Pengaduan</span></h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Mulai Pengaduan</a>
        </div>
      </div>

      <div class="col-md-6 intro-img order-md-last order-first">
        <img src="<?php echo base_url("assets/home/") ?>img/intro-img.svg" alt="" class="img-fluid">
      </div>
    </div>

  </div>
</section><!-- #intro -->

<main id="main">

  <!--==========================
      About Us Section
    ============================-->
  <section id="about">

    <div class="container">
      <div class="row">

        <div class="col-lg-5 col-md-6">
          <div class="about-img">
            <img src="<?php echo base_url("assets/home/") ?>img/about-img.jpg" alt="">
          </div>
        </div>

        <div class="col-lg-6">

          <div class="form">

            <h4>Silahkan Mengisi form untuk melakukan pengaduan</h4>
            <p>Balasan pengaduan akan dikirmkan melalui akun anda</p>
            <p style="color: blue;"><b>Data anda akan kami rahasiakan.</b></p>
            <form action="<?php echo site_url('home/do_post_pengaduan')?>" method="post">
              <div class="form-group">
                <input type="number" id = "no_nasabah" name="no_nasabah" class="form-control" id="name" placeholder="Masukan Nomor Nasabah" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="8"/>
                <div id = "validation" class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="11" data-rule="required" data-msg="Please write something for us" placeholder="Masukan Pengaduan"></textarea>
                <div class="validation"></div>
              </div>

              <div class="text-center"><button class="btn btn-primary" type="submit" title="Send Message">Laporkan</button></div>
            </form>
          </div>

        </div>
      </div>
    </div>

  </section><!-- #about -->

</main>