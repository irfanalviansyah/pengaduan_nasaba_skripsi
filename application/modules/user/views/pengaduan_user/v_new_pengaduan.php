<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pengaduan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('user') ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('user/pengaduan_user') ?>">Pengaduan</a></li>
                        <li class="breadcrumb-item active">Reply</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                    <!-- /.card -->
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Form pengaduan</h3>
                        </div>
                        <!-- /.card-header -->
                        <form action="<?php echo site_url('user/pengaduan_user/do_post') ?>" method="post">
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="text" name="subject" id="compose-textarea" class="form-control" placeholder="Masukan subject"></input>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="compose-textarea" class="form-control" style="height: 200px" placeholder="Masukan Pengaduan Anda"></textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                                </div>
                            </div>
                        </form>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>