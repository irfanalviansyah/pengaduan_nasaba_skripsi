<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dashboard User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Small Box (Stat card) -->
    <h5 class="mb-2 mt-4">Pengaduan</h5>
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?php echo $c_total_pengaduan?></h3>

            <p>Total Pengaduan</p>
          </div>
          <div class="icon">
            <i class="fas fa-table"></i>
          </div>
          <a href="<?php echo site_url('user/pengaduan_user')?>" class="small-box-footer">
            More info <i class="fas fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?php echo $c_total_pengaduan_open?></h3>

            <p>Status Pengaduan Open</p>
          </div>
          <div class="icon">
            <i class="fas fa-folder-open"></i>
          </div>
          <a href="<?php echo site_url('user/pengaduan_user')?>" class="small-box-footer">
            More info <i class="fas fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3><?php echo $c_total_pengaduan_user_reply?></h3>

            <p>Status Pengaduan User Reply</p>
          </div>
          <div class="icon">
            <i class="fas fa-reply"></i>
          </div>
          <a href="<?php echo site_url('user/pengaduan_user')?>" class="small-box-footer">
            More info <i class="fas fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3><?php echo $c_total_pengaduan_admin_reply?></h3>

            <p>Status Pengaduan Admin Reply</p>
          </div>
          <div class="icon">
            <i class="fas fa-reply"></i>
          </div>
          <a href="<?php echo site_url('user/pengaduan_user')?>" class="small-box-footer">
            More info <i class="fas fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->