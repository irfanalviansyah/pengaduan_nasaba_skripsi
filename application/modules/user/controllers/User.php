<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama_lengkap');
		if (empty($user)) redirect("auth");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
		$this->load->model('admin/admin_models');
	}

	public function index() {
		$data['c_total_pengaduan'] = $this->main_models->count('pengaduan_user','no_nasabah = '.$this->session->userdata('no_nasabah'));
		$data['c_total_pengaduan_open'] = $this->main_models->count('pengaduan_user','no_nasabah = '.$this->session->userdata('no_nasabah').' and status = "Open"');
		$data['c_total_pengaduan_user_reply'] = $this->main_models->count('pengaduan_user','no_nasabah = '.$this->session->userdata('no_nasabah').' and status = "User Reply"');
		$data['c_total_pengaduan_admin_reply'] = $this->main_models->count('pengaduan_user','no_nasabah = '.$this->session->userdata('no_nasabah').' and status = "Admin Reply"');
		$data['view'] = 'v_dashboard';
		$this->load->view("index", $data);
	}
}