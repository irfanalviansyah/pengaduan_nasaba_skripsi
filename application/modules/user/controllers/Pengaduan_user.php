<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaduan_user extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama_lengkap');
		if (empty($user)) redirect("auth");
		$url = $_SERVER['REQUEST_URI']; //request url
		$ur = explode('/', $url);
		$this->load->library('session');
		$this->load->model('main_models');
		$this->load->model('admin/admin_models');
	}

	public function index()
	{
		$data['view'] = 'pengaduan_user/v_tbl_pengaduan';
		$this->load->view("index", $data);
	}

	public function dt_pengaduan_user($no_nasabah)
	{
		$data = $this->admin_models->read_pengaduan('p.no_nasabah = ' . $no_nasabah, 'date_created DESC');
		$result = array();
		$i = 1;
		foreach ($data as $row) {
			$d = array();
			$d[] = $i++;
			$d[] = $row->no_nasabah;
			$d[] = $row->nama_lengkap;
			$d[] = $row->subject;
			$d[] = $row->message;
			$d[] = $row->status;
			$d[] = date_format(new DateTime($row->date_created), 'd-M-Y');
			$d[] = '<a href = "' . site_url('user/pengaduan_user/redir/reply_pengaduan/' . $row->id_pengaduan_m) . '" class="btn btn-warning btn_block">Lihat percakapan</a>';

			$d["DT_RowId"] = $row->id_pengaduan_m;
			$result['data'][] = $d;
		}

		if (empty($result)) $result = array("aaData" => []);
		echo json_encode($result);
	}

	public function new_pengaduan_user()
	{
		$data['view'] = 'pengaduan_user/v_new_pengaduan';
		$this->load->view("index", $data);
	}

	public function do_post_pengaduan_user()
	{
		$post = $this->input->post(NULL, TRUE);

		$no_nasabah = $this->session->userdata('no_nasabah');
		$subject = $post['subject'];
		$message = $post['message'];

		$data_pengaduan = array(
			'no_nasabah' => $no_nasabah,
			'subject' => $subject,
			'message' => $message
		);

		$this->db->trans_start();
		$this->main_models->main_create('pengaduan', $data_pengaduan);
		$last_id = $this->db->insert_id();
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$data_user_pengaduan = array(
				'id_pengaduan' => $last_id,
				'no_nasabah' => $no_nasabah,
				'status' => 'Open'
			);

			$this->db->trans_start();
			$this->main_models->main_create('pengaduan_user', $data_user_pengaduan);
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();

			if ($trans_status == FALSE) {
				$this->db->trans_rollback();
				//$this->session->set_flashdata('error', 'Gagal input Project');
				//redirect('home/admin/form_project');
				print "kesalahan database";
			} else {
				$this->db->trans_commit();
				//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
				redirect('user/pengaduan_user');
			}
		}
	}

	public function update_status_user($id)
	{
		$data_pengaduan = $this->admin_models->read_pengaduan('p.id = ' . $id);
		$status = $data_pengaduan[0]->status;
		if ($status == "Open") {
			$data = array(
				'status' => "Open"
			);

			$this->db->trans_start();
			$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $id, $data);
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();
			if ($trans_status == FALSE) {
				$this->db->trans_rollback();
				//$this->session->set_flashdata('error', 'Gagal input Project');
				//redirect('home/admin/form_project');
				print "kesalahan database";
			} else {
				$this->db->trans_commit();
				//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
				redirect('user/pengaduan_user/reply_pengaduan/' . $id);
			}
		} else {
			redirect('user/pengaduan_user/reply_pengaduan/' . $id);
		}
	}

	public function reply_pengaduan_user($id)
	{
		$data_pengaduan = $this->admin_models->read_pengaduan('p.id = ' . $id);
		$data['balasan'] = $this->admin_models->read_balasan('id_pengaduan_user = ' . $data_pengaduan[0]->id_pengaduan_user, 'pb.id DESC');

		$data['result'] = $data_pengaduan[0];
		$data['view'] = 'pengaduan_user/v_reply_pengaduan';
		$this->load->view("index", $data);
	}

	public function do_reply_pengaduan_user()
	{
		$post = $this->input->post(NULL, TRUE);
		$data = array(
			'id_user' => $post['id_user'],
			'id_pengaduan_user' => $post['id_pengaduan_user'],
			'reply' => $post['balasan']
		);

		$data_status = array(
			'status' => "User Reply"
		);

		$this->db->trans_start();
		$this->main_models->main_create('pengaduan_balasan', $data);
		$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $post['id_pengaduan_user'], $data_status);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('user/pengaduan_user/reply_pengaduan/' . $post['id_pengaduan_m']);
		}
	}

	public function do_delete_reply($id)
	{
		$this->db->trans_start();
		$this->main_models->main_delete('pengaduan_balasan', 'id = ' . $id);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function do_close_pengaduan($id){
		$data_status = array(
			'status' => "Closed"
		);

		$this->db->trans_start();
		$this->main_models->main_update('pengaduan_user', 'id_pengaduan = ' . $id, $data_status);
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if ($trans_status == FALSE) {
			$this->db->trans_rollback();
			//$this->session->set_flashdata('error', 'Gagal input Project');
			//redirect('home/admin/form_project');
			print "kesalahan database";
		} else {
			$this->db->trans_commit();
			//$this->session->set_flashdata('success', 'Berhasil input Project <code>' . $nama_proyek . '</code>');
			redirect('user/pengaduan_user');
		}
	}
}
