<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_models extends CI_Model
{

	public function main_create($table, $data)
	{
		$query = $this->db->insert($table, $data);
		if ($query) {
			return 1;
		} else {
			return $this->db->last_query();
		}
	}

	public function main_read($table = "", $where = "", $order = "")
	{
		if (!empty($where)) $this->db->where($where);
		if (!empty($order)) $this->db->order_by($order);

		$query = $this->db->get($table);
		if ($query and $query->num_rows() != 0) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function main_update($table, $where, $data)
	{
		$this->db->where($where);
		$query = $this->db->update($table, $data);
		if ($query) {
			return 1;
		} else {
			return 0;
		}
	}

	public function main_delete($table, $where)
	{
		$this->db->where($where);
		$query = $this->db->delete($table);
		if ($query) {
			return 1;
		} else {
			return 0;
		}
	}

	public function count($table, $where)
	{
		if (!empty($where)) $this->db->where($where);
		$query = $this->db->get($table);
		if ($query) {
			return $query->num_rows();
		} else {
			return array();
		}
	}
}
