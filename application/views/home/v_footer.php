<!--==========================
    Footer
  ============================-->
<footer id="footer" class="section-bg">

  <div class="container">
    <div class="copyright">
      &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
        -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<!-- <div id="preloader"></div> -->

<!-- JavaScript Libraries -->
<script src="<?php echo base_url("assets/home/") ?>lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/easing/easing.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/mobile-nav/mobile-nav.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/wow/wow.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/counterup/counterup.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url("assets/home/") ?>lib/lightbox/js/lightbox.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="<?php echo base_url("assets/home/") ?>contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="<?php echo base_url("assets/home/") ?>js/main.js"></script>

</body>

</html>

<script>
  $(window).load(function() {
    $("#no_nasabah").on('keyup', function() {
      $.ajax({
        'url': '<?php echo site_url('home/api_get_no_nasabah') ?>',
        'type': 'POST',
        'data': {
          no_nasabah: $("#no_nasabah").val()
        },
        'success': function(result) {
          if (!$.trim(result)) {
            $("#validation").html("No Nasabah anda tidak terdaftar, silahkan klik disini untuk mendaftar <a class = 'btn btn-primary btn-block' href = '<?php echo site_url('login/register')?>'>Daftar Akun</a>").css('color', 'red');
            return;
          } else {
            $("#validation").html("").css('color', 'green');
            return;

          }

        }
      });
    });
  });
</script>