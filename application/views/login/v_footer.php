<!-- jQuery -->
<script src="<?php echo base_url("assets/admin/") ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url("assets/admin/") ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets/admin/") ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets/admin/") ?>plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/admin/") ?>dist/js/adminlte.min.js"></script>

</body>

</html>

<!-- validation -->
<script>
  function validation() {
    if (document.getElementById('password').value == document.getElementById('confirm_password').value) {
      document.getElementById('message').style.color = 'green';
      document.getElementById('message').innerHTML = 'Password benar';
      document.getElementById('btn_submit').disabled = false;
    } else {
      document.getElementById('message').style.color = 'red';
      document.getElementById('message').innerHTML = 'password salah, silahkan masukan password yang benar';
      document.getElementById('btn_submit').disabled = true;
    }
  }
</script>

<script>
  function toastError(){
    toastr.error('Username Atau Password Salah')
  }
</script>

