  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io"></a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.1
    </div>
  </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="<?php echo base_url('assets/admin') ?>/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo base_url('assets/admin') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE -->
  <script src="<?php echo base_url('assets/admin') ?>/dist/js/adminlte.js"></script>

  <!-- DataTables -->
  <script src="<?php echo base_url('assets/admin') ?>/plugins/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/buttons.flash.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/jszip.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/pdfmake.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/vfs_fonts.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/buttons.html5.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/buttons.print.min.js"></script>

  <!-- OPTIONAL SCRIPTS -->
  <script src="<?php echo base_url('assets/admin') ?>/plugins/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/dist/js/demo.js"></script>
  <script src="<?php echo base_url('assets/admin') ?>/dist/js/pages/dashboard3.js"></script>

  <script src="<?php echo base_url() . 'assets/chart/raphael-min.js' ?>"></script>
  <script src="<?php echo base_url() . 'assets/chart/morris.min.js' ?>"></script>


  <!-- page script -->

  <!-- data table user -->
  <script type="text/javascript">
    $('#dt_user').dataTable({
      initComplete: function() {
        this.api().columns([6, 7]).every(function() {
          var column = this;
          var title = $(column.footer()).text();
          var select = $('<select class = "form-control"><option value="">Pilih ' + title + '</option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );

              column
                .search(val ? '^' + val + '$' : '', true, false)
                .draw();
            });

          column.data().unique().sort().each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>')
          });
        });
      },
      "ajax": {
        "url": "<?php echo site_url("admin/user_management/dt/user") ?>",
        "type": "POST"
      },
      'sScrollXInner': "150%",
      'bAutoWidth': false,
      "serverSide": false,
      "orderCellsTop": true,
      "dom": 'lBfrtip',
      "buttons": [{
          "extend": 'pdf',
          "text": 'PDF',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5]
          }
        },
        {
          "extend": 'excel',
          "text": 'Excel',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5]
          }
        },
        {
          "extend": 'print',
          "text": 'Print',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5]
          }
        }
      ]
    });
  </script>

  <!-- data table pengaduan -->
  <script type="text/javascript">
    $('#dt_pengaduan').dataTable({
      initComplete: function() {
        this.api().columns([1, 2, 5, 6]).every(function() {
          var column = this;
          var title = $(column.footer()).text();
          var select = $('<select class = "form-control"><option value="">Pilih ' + title + '</option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );

              column
                .search(val ? '^' + val + '$' : '', true, false)
                .draw();
            });

          column.data().unique().sort().each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>')
          });
        });
      },
      'rowCallback': function(row, data, index) {
        if (data[5] == "Open") {
          $(row).find('td:eq(5)').css('background-color', 'red');
          $(row).find('td:eq(5)').css('color', 'white');
        } else if (data[5] == "User Reply") {
          $(row).find('td:eq(5)').css('background-color', 'yellow');
          $(row).find('td:eq(5)').css('color', 'black');
        } else if (data[5] == "Admin Reply") {
          $(row).find('td:eq(5)').css('background-color', 'blue');
          $(row).find('td:eq(5)').css('color', 'white');
        } else if (data[5] == "Closed") {
          $(row).find('td:eq(5)').css('background-color', 'grey');
          $(row).find('td:eq(5)').css('color', 'white');
        }
      },
      "ajax": {
        "url": "<?php echo site_url("admin/pengaduan/dt/pengaduan") ?>",
        "type": "POST"
      },
      'sScrollXInner': "150%",
      'bAutoWidth': false,
      "serverSide": false,
      "orderCellsTop": true,
      "dom": 'lBfrtip',
      "buttons": [{
          "extend": 'pdf',
          "text": 'PDF',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        },
        {
          "extend": 'excel',
          "text": 'Excel',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        },
        {
          "extend": 'print',
          "text": 'Print',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        }
      ]
    });
  </script>
  <script type="text/javascript">
    $('#dt_pengaduan_user').dataTable({
      initComplete: function() {
        this.api().columns([1, 2, 5, 6]).every(function() {
          var column = this;
          var title = $(column.footer()).text();
          var select = $('<select class = "form-control"><option value="">Pilih ' + title + '</option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );

              column
                .search(val ? '^' + val + '$' : '', true, false)
                .draw();
            });

          column.data().unique().sort().each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>')
          });
        });
      },
      'rowCallback': function(row, data, index) {
        if (data[5] == "Open") {
          $(row).find('td:eq(5)').css('background-color', 'red');
          $(row).find('td:eq(5)').css('color', 'white');
        } else if (data[5] == "User Reply") {
          $(row).find('td:eq(5)').css('background-color', 'yellow');
          $(row).find('td:eq(5)').css('color', 'black');
        } else if (data[5] == "Admin Reply") {
          $(row).find('td:eq(5)').css('background-color', 'blue');
          $(row).find('td:eq(5)').css('color', 'white');
        } else if (data[5] == "Closed") {
          $(row).find('td:eq(5)').css('background-color', 'grey');
          $(row).find('td:eq(5)').css('color', 'white');
        }
      },
      "ajax": {
        "url": "<?php echo site_url("user/pengaduan_user/dt/pengaduan/" . $this->session->userdata('no_nasabah')) ?>",
        "type": "POST"
      },
      'sScrollXInner': "150%",
      'bAutoWidth': false,
      "serverSide": false,
      "orderCellsTop": true,
      "dom": 'lBfrtip',
      "buttons": [{
          "extend": 'pdf',
          "text": 'PDF',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        },
        {
          "extend": 'excel',
          "text": 'Excel',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        },
        {
          "extend": 'print',
          "text": 'Print',
          "className": 'btn btn-primary',
          "exportOptions": {
            "columns": [0, 1, 2, 3, 4, 5, 6]
          }
        }
      ]
    });
  </script>
  <!-- chart -->

  <!-- number generator -->
  <script>
    function numberGenerator() {
      document.getElementById("no_nasabah").value = Math.floor((Math.random() * 100000000) + 1);
    }
  </script>

  <!-- role function -->
  <script>
    function roleFunction() {
      var role = document.getElementById("role");
      var div = document.getElementById("DivNasabah");
      if (role.options[role.selectedIndex].text == "Admin") {
        div.style.display = "none";
      } else {
        div.style.display = "block";
      }
    }
  </script>

  <!-- change pass function -->
  <script>
    function changePass() {
      var div = document.getElementById("divPassword");
      var divc = document.getElementById("divBtnCancelPass");
      div.style.display = "block";
      divc.style.display = "block";
      document.getElementById('btn_submit').disabled = true;
    }
  </script>
  <script>
    function closePass() {
      var div = document.getElementById("divPassword");
      var divc = document.getElementById("divBtnCancelPass");
      div.style.display = "none";
      divc.style.display = "none";
      document.getElementById('btn_submit').disabled = false;
    }
  </script>

  <!-- validation -->
  <script>
    function validation() {
      if (document.getElementById('password').value == document.getElementById('confirm_password').value) {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'Password benar';
        document.getElementById('btn_submit').disabled = false;
      } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'password salah, silahkan masukan password yang benar';
        document.getElementById('btn_submit').disabled = true;
      }
    }
  </script>
  <script>
    Morris.Bar({
      element: 'graph',
      data: <?php echo $grafik_total; ?>,
      xkey: 'nrm_date',
      ykeys: ['cnt'],
      labels: ['Total Pengaduan'],
      xLabels: 'day'
    });
  </script>

  </body>

  </html>