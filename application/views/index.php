<?php
	$url = $_SERVER['REQUEST_URI']; //request url
	
	$ur = explode('/', $url);
	$home = array_search('home', $ur);
	$user = array_search('user', $ur);
	$admin = array_search('admin', $ur);
	$login = array_search('login', $ur);
	
	if(!empty($ur[$home])){ //if pemisah templat menu
		$this->load->view('home/v_headers');
		$this->load->view('home/v_topbar');

		$this->load->view($view);

		$this->load->view('home/v_footer');
	}else if(!empty($ur[$admin]) || !empty($ur[$user])){
		$this->load->view('admin/v_headers');
		$this->load->view('admin/v_topbar');
		$this->load->view('admin/v_sidebar');

		$this->load->view($view);

		$this->load->view('admin/v_footer');
	}else if(!empty($ur[$login])){
		$this->load->view('login/v_headers');

		$this->load->view($view);

		$this->load->view('login/v_footer');
	}else{
		$this->load->view('home/v_headers');
		$this->load->view('home/v_topbar');

		$this->load->view($view);

		$this->load->view('home/v_footer');
	}
