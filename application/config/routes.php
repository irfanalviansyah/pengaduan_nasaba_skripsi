<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//home routes
$route['home/api_get_no_nasabah'] = 'home/home/api_get_no_nasabah';
$route['home/do_post_pengaduan'] = 'home/home/do_post_pengaduan';

//admin route
$route['admin/berita'] = 'admin/table_berita';

//admin - user management
$route['admin/user_management'] = 'admin/user_management';
$route['admin/user_management/dt/user'] = 'admin/user_management/dt_user';
$route['admin/user_management/add_user'] = 'admin/user_management/add_user';
$route['admin/user_management/do_delete_user/(:any)'] = 'admin/user_management/do_delete_user/$1';
$route['admin/user_management/add_user/do_add_user'] = 'admin/user_management/do_add_user';
$route['admin/user_management/edit_user/(:any)/(:any)'] = 'admin/user_management/edit_user/$1';
$route['admin/user_management/do_edit_user/(:any)'] = 'admin/user_management/do_edit_user/$1';

//admin - pengaduan
$route['admin/pengaduan'] = 'admin/pengaduan';
$route['admin/pengaduan/dt/pengaduan'] = 'admin/pengaduan/dt_pengaduan';
$route['admin/pengaduan/redir/reply_pengaduan/(:any)'] = 'admin/pengaduan/update_status/$1';
$route['admin/pengaduan/reply_pengaduan/(:any)'] = 'admin/pengaduan/reply_pengaduan/$1';
$route['admin/pengaduan/do_reply_pengaduan/do_reply'] = 'admin/pengaduan/do_reply_pengaduan';
$route['admin/pengaduan/do_delete_reply/(:any)'] = 'admin/pengaduan/do_delete_reply/$1';
$route['admin/pengaduan/do_cloase_pengaduan/(:any)'] = 'admin/pengaduan/do_close_pengaduan/$1';

//user - pengaduan
$route['user/pengaduan_user'] = 'user/pengaduan_user';
$route['user/pengaduan_user/new'] = 'user/pengaduan_user/new_pengaduan_user';
$route['user/pengaduan_user/do_post'] = 'user/pengaduan_user/do_post_pengaduan_user';
$route['user/pengaduan_user/dt/pengaduan/(:any)'] = 'user/pengaduan_user/dt_pengaduan_user/$1';
$route['user/pengaduan_user/redir/reply_pengaduan/(:any)'] = 'user/pengaduan_user/update_status_user/$1';
$route['user/pengaduan_user/reply_pengaduan/(:any)'] = 'user/pengaduan_user/reply_pengaduan_user/$1';
$route['user/pengaduan_user/do_reply_pengaduan/do_reply'] = 'user/pengaduan_user/do_reply_pengaduan_user';
$route['user/pengaduan_user/do_delete_reply/(:any)'] = 'user/pengaduan_user/do_delete_reply/$1';
$route['user/pengaduan_user/do_cloase_pengaduan/(:any)'] = 'user/pengaduan_user/do_close_pengaduan/$1';

//Login ROUTEs
$route['login'] = 'admin/login';
$route['login/do_login'] = 'admin/login/do_login';
$route['login/do_logout/(:any)'] = 'admin/login/do_logout/$1';
$route['login/register'] = 'admin/login/register';
$route['login/register/do_register'] = 'admin/login/do_register';

//User ROUTES
$route['user'] = 'user';
